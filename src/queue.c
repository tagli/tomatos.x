/**
 * @file queue.c
 * @author Gokce Taglioglu
 */

#include "queue.h"
#include <string.h>

//==============================================================================
// Global Functions
//==============================================================================

void Queue_create(Queue* self, byte* memory, qsize_t itemSize, qsize_t maxItems)
{
    self->memory = memory;
    self->itemSize = itemSize;
    self->nMaxItems = maxItems;
    self->nItems = 0;
    self->headIdx = 0;
    self->tailIdx = 0;
}

void Queue_reset(Queue* self)
{
    self->nItems = 0;
    self->headIdx = 0;
    self->tailIdx = 0;
}

Result Queue_send(Queue* self, const void* item)
{
    if (self->nItems == self->nMaxItems) {
        return Result_Err;
    }
    byte* headPtr = self->memory + (self->headIdx * self->itemSize);
    memcpy(headPtr, item, self->itemSize);
    ++(self->headIdx);
    if (self->headIdx == self->nMaxItems) self->headIdx = 0;
    ++(self->nItems);
    return Result_Ok;
}

Result Queue_overwrite(Queue* self, const void* item)
{
    byte* headPtr = self->memory + (self->headIdx * self->itemSize);
    memcpy(headPtr, item, self->itemSize);
    ++(self->headIdx);
    if (self->headIdx == self->nMaxItems) self->headIdx = 0;

    if (self->nItems == self->nMaxItems) {
        ++(self->tailIdx);
        if (self->tailIdx == self->nMaxItems) self->tailIdx = 0;
    }
    else {
        ++(self->nItems);
    }

    return Result_Ok;
}

Result Queue_receive(Queue* self, void* item)
{
    if (self->nItems == 0) {
        return Result_None;
    }
    byte* tailPtr = self->memory + (self->tailIdx * self->itemSize);
    memcpy(item, tailPtr, self->itemSize);
    ++(self->tailIdx);
    if (self->tailIdx == self->nMaxItems) self->tailIdx = 0;
    --(self->nItems);
    return Result_Some;
}

bool Queue_isEmpty(const Queue* self)
{
    if (self->nItems == 0) return true;
    else return false;
}

