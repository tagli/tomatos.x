/**
 * @file flash2.h
 * @author Gokce Taglioglu
 */

#ifndef FLASH_H
#define	FLASH_H

#include <xc.h>
#include <stdint.h>

//==============================================================================
// Definitions & Settings
//==============================================================================

#define FLASH_EMPTY_CELL ((uint8_t)0xff)
#if _ROMSIZE > 65536
typedef uint32_t flash_adr_t;
#else
typedef uint16_t flash_adr_t;
#endif

//==============================================================================
// Global Functions
//==============================================================================

void flash2_eraseBlock(flash_adr_t blockAdr);
void flash2_write(flash_adr_t targetAdr, const void* source, size_t size);
void flash2_read(flash_adr_t sourceAdr, void* target, size_t bufSize);

#endif	/* FLASH_H */

