/**
 * @file eeprom2.h
 * @author Gokce Taglioglu
 */

#ifndef EEPROM2_H
#define	EEPROM2_H

#include <stdbool.h>
#include "common_defs.h"
#include "kernel.h"

//==============================================================================
// Definitions & Settings
//==============================================================================

#define EEPROM_SIZE 256
#define EEPROM_EMPTY_CELL ((uint8_t)0xff)

#if EEPROM_SIZE <= 256
typedef uint8_t epr_adr_t;
#else
typedef uint16_t epr_adr_t;
#endif
typedef epr_adr_t epr_size_t;

struct EepromRequest {
    Event super;
    Machine* client;
    byte* buffer;
    epr_adr_t adr;
    epr_size_t size;
};
typedef struct EepromRequest EprWriteReq;
typedef struct EepromRequest EprReadReq;

//==============================================================================
// Global Functions
//==============================================================================

void eeprom2_initialize(void);
#define eeprom2_write(req_) eeprom2_request((req_))
#define eeprom2_read(req_) eeprom2_request((req_))
void eeprom2_request(struct EepromRequest* req);

// Raw operations, UNSAFE after the kernel starts
void eeprom2_read_raw(epr_adr_t adr, void* target, epr_size_t size);
void eeprom2_write_raw(epr_adr_t adr, const void* source, epr_size_t size);

//==============================================================================
// Interrupt Functions
//==============================================================================

void eeprom2_writeCompleteISR(void);

#endif	/* EEPROM2_H */

