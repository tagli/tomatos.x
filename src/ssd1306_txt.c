/**
 * @file ssd1306_txt.c
 * @author Gokce Taglioglu
 */

#include "ssd1306_txt.h"
#include <string.h>
#include "kernel.h"
#include "diagnostics.h"
#include "eventq.h"
#include "6x8.font"
#include "i2c.h"

//==============================================================================
// Local Definitions & Settings
//==============================================================================

#define MACHINE_PRIORITY 0
#define REQ_QUEUE_LENGTH 4 // used for jobs received during busy time

#define SCREEN_WIDTH 128 // pixels
#define SCREEN_HEIGHT 64 // pixels
#define SCREEN_COLUMNS ((SCREEN_WIDTH) / (BLOCK_WIDTH))
#define SCREEN_ROWS ((SCREEN_HEIGHT) / (FONT_HEIGHT))

#define SLAVE_ADR 0x3c // SSD1306 7-bit address
#define BUFFER_SIZE ((BLOCK_WIDTH) + 1)

struct PrintStrReq {
    Event super;
    uint8_t row;
    uint8_t column;
    const char* str;
};
typedef struct PrintStrReq PrintStrReq;

// Sub State Variable Definitions

struct PrintStrVars {
    uint8_t row;
    uint8_t column;
    const char* chPtr;
};

struct ClearVars {
    uint8_t row;
    uint8_t column;
    uint8_t counter8x8;
};

//==============================================================================
// Local Function Prototypes
//==============================================================================

// State Functions
static void state_initialize(Event* event);
static void state_idle(Event* event);
static void state_processPendingReqs(Event* event);
static void state_printStr(Event* event);
static void state_printRaw(Event* event);
static void state_clearing(Event* event);

// Other Helper Functions
static void serveRequest(Event* event);
static void returnToBeginning(void);
static void moveCursor(uint8_t row, uint8_t column);
static void printChar(char ch);
static void clear8x8(void);

//==============================================================================
// Local Variables
//==============================================================================

static const uint8_t initCmd[] = {
    0x00,
    0xae, // Screen off
    0xc8, // COM scan direction inverted
    0xa1, // Segment remap inverted
    0xda, 0x12, // COM pins HW config
    0x8d, 0x14, // Charge pump on
    0xaf // Screen on
};

static Machine machine = {&state_initialize, MACHINE_PRIORITY};
static Event* reqBuffer[REQ_QUEUE_LENGTH] = {NULL};
static EventQ reqQ = {0};
static Event Event_Ssd1306_ClearAllReq = STATIC_EVENT(EventType_Ssd1306_ClearAllReq);

static union {
    struct PrintStrVars printStr;
    struct ClearVars clear;
} subState;

static uint8_t txBuffer[10] = {0};
_Static_assert(sizeof(txBuffer) >= sizeof(initCmd), "Buffer size insufficient");

static I2cXferReq i2cReq = {
    .super = STATIC_EVENT(EventType_I2C_WriteReq),
    .client = &machine,
    .slaveAdr = SLAVE_ADR,
};

//==============================================================================
// Global Functions
//==============================================================================

void ssd1306_initialize(void)
{
    i2c_initialize();
    EventQ_create(&reqQ, reqBuffer, REQ_QUEUE_LENGTH);
    machine.stateFunction = &state_initialize;
    // For some reason, I2C can't access initCmd from ROM directly.
    memcpy(txBuffer, initCmd, sizeof(initCmd));
    i2cReq.client = &machine;
    i2cReq.source = (const byte*)txBuffer;
    i2cReq.size = sizeof(initCmd);
    i2c_request(&i2cReq);
    ssd1306_clearAll();
}

void ssd1306_printStr(uint8_t row, uint8_t column, const char* str)
{
    PrintStrReq* request = (PrintStrReq*)Event_create(
            EventType_Ssd1306_PrintStrReq, sizeof(PrintStrReq));
    request->row = row;
    request->column = column;
    request->str = str;
    kernel_push(&machine, (Event*)request);
}

void ssd1306_clearAll(void)
{
    kernel_push(&machine, (Event*)&Event_Ssd1306_ClearAllReq);
}

//==============================================================================
// State Functions
//==============================================================================

static void state_initialize(Event* event)
{
    switch (event->type) {
    case EventType_Ssd1306_PrintStrReq:
    case EventType_Ssd1306_PrintRawReq:
    case EventType_Ssd1306_ClearAllReq:
        EventQ_push(&reqQ, event);
        break;
    case EventType_I2C_RespOk:
        returnToBeginning();
        break;
    default:
        ASSERT(false);
        break;
    }
}

static void state_idle(Event* event)
{
    switch (event->type) {
    case EventType_Ssd1306_PrintStrReq:
    case EventType_Ssd1306_PrintRawReq:
    case EventType_Ssd1306_ClearAllReq:
        serveRequest(event);
        break;
    default:
        ASSERT(false);
        break;
    }
}

static void state_processPendingReqs(Event* event)
{
    switch (event->type) {
    case EventType_Ssd1306_PrintStrReq:
    case EventType_Ssd1306_PrintRawReq:
    case EventType_Ssd1306_ClearAllReq:
        EventQ_push(&reqQ, event);
        break;

    case EventType_Entry:
    {
        Event* poppedEvent = EventQ_pop(&reqQ);
        serveRequest(poppedEvent);
        Event_releaseDestroy(poppedEvent);
        break;
    }

    default:
        ASSERT(false);
        break;
    }
}

static void state_printStr(Event* event)
{
    switch (event->type) {
    case EventType_Ssd1306_PrintStrReq:
    case EventType_Ssd1306_PrintRawReq:
    case EventType_Ssd1306_ClearAllReq:
        EventQ_push(&reqQ, event);
        break;

    case EventType_I2C_RespOk:
        // Printing is complete
        if (*subState.printStr.chPtr == '\0') {
            returnToBeginning();
        }
        // Printing continues
        else {
            // Advance to the next line if necessary
            if (subState.printStr.column == SCREEN_COLUMNS) {
                subState.printStr.column = 0;
                if (++subState.printStr.row == SCREEN_ROWS) subState.printStr.row = 0;
                moveCursor(subState.printStr.row, 0);
            }
            else {
                printChar(*subState.printStr.chPtr++);
                ++subState.printStr.column;
            }
        }
        break;

    case EventType_I2C_RespErr:
        returnToBeginning();
        break;

    default:
        ASSERT(false);
        break;
    }
}

static void state_clearing(Event* event)
{
    switch (event->type) {
    case EventType_Ssd1306_PrintStrReq:
    case EventType_Ssd1306_PrintRawReq:
    case EventType_Ssd1306_ClearAllReq:
        EventQ_push(&reqQ, event);
        break;

    case EventType_I2C_RespOk:
        // Clearing is complete
        if (subState.clear.counter8x8 == 128) {
            returnToBeginning();
        }
        // Clearing continues
        else {
             // Advance to the next line if necessary
            if (subState.clear.column == 16) {
                subState.clear.column = 0;
                ++subState.clear.row;
                moveCursor(subState.clear.row, 0);
            }
            else {
                clear8x8();
                ++subState.clear.column;
                ++subState.clear.counter8x8;
            }
        }
        break;

    case EventType_I2C_RespErr:
        returnToBeginning();
        break;

    default:
        ASSERT(false);
        break;
    }
}

//==============================================================================
// Local Helper Functions
//==============================================================================

static void serveRequest(Event* event)
{
    switch (event->type) {
    case EventType_Ssd1306_PrintStrReq:
    {
        PrintStrReq* req = (PrintStrReq*)event;
        subState.printStr.chPtr = req->str;
        subState.printStr.row = req->row;
        subState.printStr.column = req->column;
        machine.stateFunction = &state_printStr;
        moveCursor(subState.printStr.row, subState.printStr.column);
        break;
    }

    case EventType_Ssd1306_PrintRawReq:
        // TODO: Implement
        break;

    case EventType_Ssd1306_ClearAllReq:
    {
        subState.clear.counter8x8 = 0;
        subState.clear.row = 0;
        subState.clear.column = 0;
        machine.stateFunction = &state_clearing;
        moveCursor(0, 0);
        break;
    }

    default:
        ASSERT(false);
        break;
    } // end switch
}

static void returnToBeginning(void)
{
    if (EventQ_isEmpty(&reqQ)) {
        machine.stateFunction = &state_idle;
    }
    else {
        machine.stateFunction = &state_processPendingReqs;
        kernel_push(&machine, &Event_Entry);
    }
}

static void moveCursor(uint8_t row, uint8_t column)
{
    row &= 0x07;
    uint8_t lcdColumn = (column * BLOCK_WIDTH) & 0x7f;

    txBuffer[0] = 0x00; // control byte
    txBuffer[1] = lcdColumn & 0x0f; // column start adr low
    txBuffer[2] = 0x10 | (lcdColumn >> 4); // column start adr high
    txBuffer[3] = 0xb0 | row;

    i2cReq.source = txBuffer;
    i2cReq.size = 4;
    i2c_request(&i2cReq);
}

static void printChar(char ch)
{
    uint8_t fontIdx = (uint8_t)ch - FONT_OFFSET;
    txBuffer[0] = 0x40;
    txBuffer[1] = 0;
    memcpy(&txBuffer[2], &FONT_DATA[fontIdx][0], FONT_WIDTH);

    i2cReq.source = txBuffer;
    i2cReq.size = BLOCK_WIDTH + 1;
    i2c_request(&i2cReq);
}

static void clear8x8(void)
{
    txBuffer[0] = 0x40;
    memset(&txBuffer[2], 0, 8);

    i2cReq.source = txBuffer;
    i2cReq.size = 8 + 1;
    i2c_request(&i2cReq);
}

