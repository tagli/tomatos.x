/**
 * @file nvsimple.h
 * @author Gokce Taglioglu
 */

#ifndef NVSIMPLE_H
#define	NVSIMPLE_H

#include <stdint.h>
#include "common_defs.h"
#include "kernel.h"

//==============================================================================
// Definitions & Settings
//==============================================================================

struct NonVolatile {
    uint32_t test0;
    uint16_t test1;
};
typedef struct NonVolatile NonVolatile;

enum NvSimpleState {
    NvSimpleState_Free, // No operation is ongoing
    NvSimpleState_ReadOnly, // Store operation is in progress
    NvSimpleState_Locked, // Recall operation is in progress
};
typedef enum NvSimpleState NvSimpleState;

//==============================================================================
// Global Functions
//==============================================================================

Result nvsimple_initialize(void);
Result nvsimple_store(Machine* client);
Result nvsimple_recall(Machine* client);
NvSimpleState nvsimple_getState(void);

//==============================================================================
// Global Variables
//==============================================================================

extern NonVolatile nvsimple_data;

#endif	/* NVSIMPLE_H */

