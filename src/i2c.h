/**
 * @file i2c.h
 * @author Gokce Taglioglu
 */

#ifndef I2C_H
#define	I2C_H

#include <stddef.h>
#include "kernel.h"

//==============================================================================
// Definitions & Settings
//==============================================================================

struct I2cXferReq {
    Event super;
    Machine* client;
    uint8_t slaveAdr;
    union {
        const byte* source;
        byte* target;
    };
    size_t size;
};
typedef struct I2cXferReq I2cXferReq;

struct I2cRespErr {
    Event super;
    size_t nBytesXfered;
};
typedef struct I2cRespErr I2cRespErr;

//==============================================================================
// Global Functions
//==============================================================================

void i2c_initialize(void);
void i2c_request(I2cXferReq* req);

//==============================================================================
// Interrupt Functions
//==============================================================================

void i2c_ISR(void);

#endif	/* I2C_H */

