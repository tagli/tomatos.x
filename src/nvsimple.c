/**
 * @file nvsimple.c
 * @author Gokce Taglioglu
 */

#include "nvsimple.h"
#include <stdbool.h>
#include <string.h>
#include "diagnostics.h"
#include "eeprom2.h"

//==============================================================================
// Local Definitions & Settings
//==============================================================================

#define MACHINE_PRIORITY 0
#define N_BLOCKS (EEPROM_SIZE / (sizeof(nvsimple_data) + sizeof(cycle_t)))
typedef uint8_t cycle_t;
typedef uint8_t blk_idx_t;

//==============================================================================
// Local Function Prototypes
//==============================================================================

// State Functions
static void state_processing(Event* event);

//==============================================================================
// Local Variables
//==============================================================================

static Machine machine = {&state_processing, MACHINE_PRIORITY};
static Machine* currentClient = NULL;
static Event Event_NvSimpleRequestComplete = STATIC_EVENT(EventType_NvSimpleRequestComplete);
static blk_idx_t currentIdx = 0;
static cycle_t currentCycle = 0;
static bool eepromEmpty = false;
static NvSimpleState state = NvSimpleState_Free;

//==============================================================================
// Global Variables
//==============================================================================

NonVolatile nvsimple_data = {0};

//==============================================================================
// Global Functions
//==============================================================================

Result nvsimple_initialize(void)
{
    eeprom2_initialize();
    cycle_t cycleNow;

    // Blank EEPROM is special case
    eeprom2_read_raw(0, &cycleNow, sizeof(cycleNow));
    if (cycleNow == (cycle_t)EEPROM_EMPTY_CELL) {
        currentIdx = 0;
        currentCycle = (cycle_t)(EEPROM_EMPTY_CELL + 1);
        eepromEmpty = true;
        memset(&nvsimple_data, 0, sizeof(nvsimple_data));
        return Result_None;
    }

    // Find the index of the most recent entry
    eepromEmpty = false;
    cycle_t cyclePre = cycleNow;
    for (blk_idx_t idx = 1; idx < N_BLOCKS; ++idx) {
        eeprom2_read_raw(idx, &cycleNow, sizeof(cycleNow));
        if (cycleNow == (cycle_t)EEPROM_EMPTY_CELL
                || (cycle_t)(cycleNow - cyclePre) > 2)
        {
            currentIdx = idx - 1;
            currentCycle = cyclePre;
            break;
        }
        // needed for (idx == N_BLOCKS) - 1 case
        else {
            currentIdx = idx;
            currentCycle = cycleNow;
        }
        cyclePre = cycleNow;
    }

    // Obtain the most recent data
    epr_adr_t adr = N_BLOCKS + (currentIdx * sizeof(nvsimple_data));
    eeprom2_read_raw(adr, &nvsimple_data, sizeof(nvsimple_data));
    return Result_Some;
}

Result nvsimple_store(Machine* client)
{
    if (state != NvSimpleState_Free) return Result_Err;

    static EprWriteReq dataWriteReq = {
        .super.type = EventType_EepromWriteRequest,
        .buffer = (byte*)&nvsimple_data,
        .client = NULL,
        .size = sizeof(nvsimple_data),
    };
    static EprWriteReq cycleWriteReq = {
        .super.type = EventType_EepromWriteRequest,
        .buffer = (byte*)&currentCycle,
        .client = &machine,
        .size = sizeof(currentCycle),
    };

    if (eepromEmpty) {
        currentIdx = 0;
        currentCycle = (cycle_t)(EEPROM_EMPTY_CELL + 1);
    }
    else {
        if (++currentIdx == N_BLOCKS) currentIdx = 0;
        if (++currentCycle == (cycle_t)EEPROM_EMPTY_CELL) ++currentCycle;
    }

    eepromEmpty = false;
    currentClient = client;
    state = NvSimpleState_ReadOnly;
    dataWriteReq.adr = N_BLOCKS + (currentIdx * sizeof(nvsimple_data));
    cycleWriteReq.adr = currentIdx;
    eeprom2_write(&dataWriteReq);
    eeprom2_write(&cycleWriteReq);

    return Result_Ok;
}

Result nvsimple_recall(Machine* client)
{
    if (state != NvSimpleState_Free) return Result_Err;
    if (eepromEmpty) {
        memset(&nvsimple_data, 0, sizeof(nvsimple_data));
        if (client != NULL) {
            kernel_push(client, &Event_NvSimpleRequestComplete);
        }
        return Result_Ok;
    }

    static EprWriteReq dataReadReq = {
        .super.type = EventType_EepromReadRequest,
        .buffer = (byte*)&nvsimple_data,
        .client = &machine,
        .size = sizeof(nvsimple_data),
    };

    state = NvSimpleState_Locked;
    currentClient = client;
    dataReadReq.adr = N_BLOCKS + (currentIdx * sizeof(nvsimple_data));
    eeprom2_read(&dataReadReq);
    return Result_Ok;
}

NvSimpleState nvsimple_getState(void)
{
    return state;
}

//==============================================================================
// State Functions
//==============================================================================

static void state_processing(Event* event)
{
    ASSERT(event->type == EventType_EepromRequestComplete);
    state = NvSimpleState_Free;
    if (currentClient != NULL) {
        kernel_push(currentClient, &Event_NvSimpleRequestComplete);
    }
}

