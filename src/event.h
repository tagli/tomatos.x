/**
 * @file event.h
 * @author Gokce Taglioglu
 */

#ifndef EVENT_H
#define EVENT_H

#include <stdint.h>
#include "event_types.h"

//==============================================================================
// Definitions & Settings
//==============================================================================

struct Event {
    EventType type;
    int8_t refs;
};
typedef struct Event Event;

//==============================================================================
// Macros
//==============================================================================

#define STATIC_EVENT(type_) {(type_), -1}

//==============================================================================
// Global Functions
//==============================================================================

void Event_initialize(void);
void Event_hold(Event* event);
Event* Event_create(EventType type, uint8_t size);
void Event_releaseDestroy(Event* event);

//==============================================================================
// Global Common Events
//==============================================================================

extern Event Event_None;
extern Event Event_Entry;
extern Event Event_Reset;
extern Event Event_Continue;

#endif	/* EVENT_H */

