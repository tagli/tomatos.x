/**
 * @file modbus.h
 * @author Gokce Taglioglu
 */

#ifndef MODBUS_H
#define	MODBUS_H

#include <stdint.h>
#include <stdbool.h>
#include "kernel.h"

//==============================================================================
// Definitions & Settings
//==============================================================================

struct Dictionary {
    uint16_t startIndex;
    uint16_t nRegs;
    uint16_t* regs;
    Machine* owner;
};
typedef struct Dictionary Dictionary;

//==============================================================================
// Global Functions
//==============================================================================

void modbus_initialize(void);
void modbus_addHoldingDictionary(const Dictionary* dict);
void modbus_addInputDictionary(const Dictionary* dict);

//==============================================================================
// Interrupt Functions
//==============================================================================

void modbus_timeoutISR(void);
void modbus_rxISR(uint8_t rxByte);
void modbus_txISR(void);

#endif	/* MODBUS_H */

