/**
 * @file eventq.h
 * @author Gokce Taglioglu
 */

#ifndef EVENTQ_H
#define	EVENTQ_H

#include <stdint.h>
#include "common_defs.h"
#include "event.h"
#include "queue.h"

//==============================================================================
// Definitions
//==============================================================================

typedef Queue EventQ;

//==============================================================================
// Global Functions
//==============================================================================

void EventQ_create(EventQ* self, Event** buffer, qsize_t maxEvents);
Result EventQ_push(EventQ* self, Event* event);
Event* EventQ_pop(EventQ* self);

//==============================================================================
// Macros
//==============================================================================

#define EventQ_isEmpty(self_) \
    Queue_isEmpty((self_))

#endif	/* EVENTQ_H */

