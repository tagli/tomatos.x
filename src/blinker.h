/**
 * @file blinker.h
 * @author Gokce Taglioglu
 */

#ifndef BLINKER_H
#define	BLINKER_H

//==============================================================================
// Global Functions
//==============================================================================

void blinker_initialize(void);

#endif	/* BLINKER_H */

