/**
 * @file kernel.c
 * @author Gokce Taglioglu
 */

#include "kernel.h"
#include <xc.h>
#include "diagnostics.h"
#include "common_defs.h"
#include "queue.h"

//==============================================================================
// Local Definitions & Settings
//==============================================================================

#define MAX_LOW_PRI_EVENTS 10
#define MAX_HIGH_PRI_EVENTS 10

struct DirectedEvent {
    Machine* target;
    Event* event;
};
typedef struct DirectedEvent DirectedEvent;

//==============================================================================
// Local Variables
//==============================================================================

static DirectedEvent bufferPriLow[MAX_LOW_PRI_EVENTS];
static DirectedEvent bufferPriHigh[MAX_HIGH_PRI_EVENTS];
static Queue allQueues[MACHINE_MAX_PRIORITY + 1];

//==============================================================================
// Global Functions
//==============================================================================

void kernel_initialize(void)
{
    Queue_create(&allQueues[0], (byte*)bufferPriLow, sizeof(DirectedEvent),
                 MAX_LOW_PRI_EVENTS);
    Queue_create(&allQueues[1], (byte*)bufferPriHigh, sizeof(DirectedEvent),
                 MAX_HIGH_PRI_EVENTS);
    Event_initialize();
}

void kernel_push(Machine* machine, Event* event)
{
    Event_hold(event);
    DirectedEvent dEvent = {machine, event};
    di();
    Result result = Queue_send(&allQueues[machine->priority], &dEvent);
    ASSERT(result == Result_Ok);
    ei();
}

void kernel_pushFromISR(Machine* machine, Event* event)
{
    Event_hold(event);
    DirectedEvent dEvent = {machine, event};
    Result result = Queue_send(&allQueues[machine->priority], &dEvent);
    ASSERT(result == Result_Ok);
}

Result kernel_pop(Machine** machine, Event** event)
{
    Result popResult;
    DirectedEvent dEvent;
    for (int8_t i = MACHINE_MAX_PRIORITY; i >= 0; --i) {
        di();
        popResult = Queue_receive(&allQueues[i], &dEvent);
        ei();
        if (popResult == Result_Some) {
            *machine = dEvent.target;
            *event = dEvent.event;
            return Result_Some;
        }
    }
    return Result_None; // All queues are empty
}

