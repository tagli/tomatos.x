/**
 * @file common_defs.h
 * @author Gokce Taglioglu
 */

#ifndef COMMON_DEFS_H
#define	COMMON_DEFS_H

typedef unsigned char byte;

enum Result {
    Result_Err = 0,
    Result_None = 0,
    Result_Ok = 1,
    Result_Some = 1,
};
typedef enum Result Result;

#endif	/* COMMON_DEFS_H */

