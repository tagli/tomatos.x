/**
 * @file queue.h
 * @author Gokce Taglioglu
 */

#ifndef QUEUE_H
#define	QUEUE_H

#include <stdint.h>
#include <stdbool.h>
#include "common_defs.h"

//==============================================================================
// Definitions
//==============================================================================

typedef uint8_t qsize_t;

struct Queue {
    byte* memory;
    qsize_t itemSize;
    qsize_t nMaxItems;
    qsize_t nItems;
    qsize_t headIdx;
    qsize_t tailIdx;
};
typedef struct Queue Queue;

//==============================================================================
// Global Functions
//==============================================================================

void Queue_create(Queue* self, byte* memory, qsize_t itemSize, qsize_t maxItems);
void Queue_reset(Queue* self);
Result Queue_send(Queue* self, const void* item);
Result Queue_overwrite(Queue* self, const void* item);
Result Queue_receive(Queue* self, void* item);
bool Queue_isEmpty(const Queue* self);

#endif	/* QUEUE_H */

