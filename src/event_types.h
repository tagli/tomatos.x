/**
 * @file event_types.h
 * @author Gokce Taglioglu
 */

#ifndef EVENT_TYPES_H
#define	EVENT_TYPES_H

//==============================================================================
// Global Event Type Allocation
//==============================================================================

enum EventType {

    // Common Events
    EventType_None = 0,
    EventType_Entry,
    EventType_Reset,
    EventType_Continue,

    // Delay Notifier
    EventType_SysTick, // internal
    EventType_Wakeup,

    // Modbus
    EventType_UsartRxComplete, // internal

    // EEPROM
    EventType_EepromWriteRequest,
    EventType_EepromReadRequest,
    EventType_EepromRequestComplete,
    EventType_EepromWriteComplete, // internal

    // nvsimple
    EventType_NvSimpleRequestComplete,

    // I2C Manager
    EventType_I2C_WriteReq,
    EventType_I2C_ReadReq,
    EventType_I2C_RespOk,
    EventType_I2C_RespErr,
    EventType_I2C_SlaveNak, // internal
    EventType_I2C_XferComplete, // internal

    // SSD1306
    EventType_Ssd1306_PrintStrReq,
    EventType_Ssd1306_PrintRawReq,
    EventType_Ssd1306_ClearAllReq,
};
typedef enum EventType EventType;

#endif	/* EVENT_TYPES_H */

