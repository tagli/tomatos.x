/**
 * @file wakeup.h
 * @author Gokce Taglioglu
 */

#ifndef WAKEUP_H
#define	WAKEUP_H

#include <stdint.h>
#include <stdbool.h>
#include "kernel.h"
#include "common_defs.h"

//==============================================================================
// Definitions & Settings
//==============================================================================

#define CLOCK_FREQUENCY 5000000ULL // in MIPS, not raw Hz

//==============================================================================
// Global Functions
//==============================================================================

void wakeup_initialize(void);
void wakeup_tickFromISR(void);
Result wakeup_request(Machine* client, uint16_t tick);

#endif	/* WAKEUP_H */

