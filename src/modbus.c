/**
 * @file modbus.c
 * @author Gokce Taglioglu
 */

#include "modbus.h"
#include <xc.h>
#include <string.h>
#include "crc.h"
#include "common_defs.h"
#include "diagnostics.h"

//==============================================================================
// Settings & Macros
//==============================================================================

#define MACHINE_PRIORITY 0
#define MAX_HOLDING_DICTS 4
#define MAX_INPUT_DICTS 4
#define BUFFER_SIZE 40
#define DEFAULT_ADDRESS 1
#define INVALID_ADDRESS 255
#define DE_PIN LATBbits.LATB1 // TODO: Placeholder!!!

#define TMR_COUNTER TMR0
#define TMR_PRELOAD 114 // B19200, 1:64 @ 20 MHz
#define TMR_CLEAR_FLAG() INTCONbits.T0IF = 0
#define TMR_EI() INTCONbits.T0IE = 1
#define TMR_DI() INTCONbits.T0IE = 0

#define ENABLE_RECEIVER() \
    do { \
        buffer.bytes[0] = INVALID_ADDRESS; \
        RCSTAbits.CREN = 1; \
    } while (0)

#define BYTE_SWAP_16(u16) \
    (uint16_t)((((uint16_t)(u16)) << 8) | (((uint16_t)(u16)) >> 8))

//==============================================================================
// Definitions
//==============================================================================

enum FuncCode {
    FuncCode_ReadHolding = 3,
    FuncCode_ReadInput = 4,
    FuncCode_WriteSingle = 6,
    FuncCode_WriteMultiple = 16,
};
typedef enum FuncCode FuncCode;

enum ExCode {
    ExCode_IllegalFunc = 1,
    ExCode_IllegalDataAdr = 2,
    ExCode_IllegalDataValue = 3,
    ExCode_DeviceFailure = 4,
};
typedef enum ExCode ExCode;

union Buffer {
    uint8_t bytes[BUFFER_SIZE];

    __pack struct {
        uint8_t address;
        uint8_t function;
        uint16_t index_rev;
        union {
            uint16_t nRegs_rev; // Used in 3 & 4
            uint16_t value_rev; // Used in 6
        };
        uint16_t crc;
    } request_3_4_6, request_3_4, request_6, response_16;

    __pack struct {
        uint8_t address;
        uint8_t function;
        uint16_t index_rev;
        uint16_t nRegs_rev;
        uint8_t nBytes;
        uint16_t firstData_rev; // placeholder
        // CRC location depends on package size
    } request_16;

    __pack struct {
        uint8_t address;
        uint8_t function;
        uint8_t nBytes;
        uint16_t firstData_rev; // placeholder
        // CRC location depends on package size
    } response_3_4;

    __pack struct {
        uint8_t address;
        uint8_t function;
        uint8_t code;
        uint16_t crc;
    } exception;
};
typedef union Buffer Buffer;

//==============================================================================
// Local Function Prototypes
//==============================================================================

// State Functions
static void entryState(Event* event);
static void function_3_4(Event* event);
static void function_6(Event* event);
static void function_16(Event* event);

// Dictionary Member Functions
static bool Dictionary_owns(const Dictionary* dict, uint16_t regIndex,
        uint16_t nRegs);
static void Dictionary_writeFromBuffer(const Dictionary* dict, uint16_t regIndex,
        uint16_t nRegs, const byte* buffer);
static void Dictionary_readToBuffer(const Dictionary* dict, uint16_t regIndex,
        uint16_t nRegs, byte* buffer);

// Other Helper Functions
static const Dictionary* findInputDictionary(uint16_t regIndex, uint16_t nRegs);
static const Dictionary* findHoldingDictionary(uint16_t regIndex, uint16_t nRegs);
static bool isCrcValid(void);
static void sendException(ExCode code);

//==============================================================================
// Local Variables
//==============================================================================

static Machine machine = {&entryState, MACHINE_PRIORITY};
static uint8_t address = DEFAULT_ADDRESS;
static Buffer buffer = {0};
static uint8_t nTxRemaining = 0; // Bytes remaining in TX buffer
static uint8_t* txPtr = NULL;

// Registered Dictionaries
static const Dictionary* holdingDicts[MAX_HOLDING_DICTS] = {NULL};
static const Dictionary* inputDicts[MAX_INPUT_DICTS] = {NULL};
static uint8_t nHoldingDicts = 0;
static uint8_t nInputDicts = 0;

// Variables shared by ISRs, but not normal functions
static uint8_t rxIndex = 0;
static bool rxOverflowed = false;

//==============================================================================
// Global Functions
//==============================================================================

void modbus_initialize(void)
{
    // Reset the state
    nHoldingDicts = 0;
    nInputDicts = 0;
    buffer.bytes[0] = INVALID_ADDRESS;
    machine.stateFunction = &entryState;

    // USART Hardware Settings
    TRISCbits.TRISC6 = 0; // TX pin
    TRISCbits.TRISC7 = 1; // RX pin
    SPBRG = 64; // B19200, BRGH = 1 @ 20 MHz
    TXSTA = 0;
    TXSTA |= _TXSTA_BRGH_MASK // High speed baud rate generation
            | _TXSTA_TXEN_MASK; // Transmitter enable
    RCSTA = 0;
    RCSTA |= _RCSTA_SPEN_MASK // Enable serial port
            | _RCSTA_CREN_MASK; // Enable receiver

    // Timer Settings
    T0CON = 0;
    T0CON |= (0b101u << _T0CON_T0PS0_POSN) // prescaler = 1:64
            | _T0CON_T08BIT_MASK // 8-bit mode
            | _T0CON_TMR0ON_MASK; // Enable timer

    // Interrupt Settings
    PIE1bits.RCIE = 1;
    // TMR interrupt is enabled in USART RX ISR
}

void modbus_addHoldingDictionary(const Dictionary* dict)
{
    ASSERT(nHoldingDicts < MAX_HOLDING_DICTS);
    holdingDicts[nHoldingDicts++] = dict;
}

void modbus_addInputDictionary(const Dictionary* dict)
{
    ASSERT(nInputDicts < MAX_INPUT_DICTS);
    inputDicts[nInputDicts++] = dict;
}

//==============================================================================
// Interrupt Functions (called by ISR)
//==============================================================================

void modbus_timeoutISR(void)
{
    static Event Event_UsartRxComplete = STATIC_EVENT(EventType_UsartRxComplete);
    TMR_DI();
    if (!rxOverflowed && (buffer.bytes[0] == address || buffer.bytes[0] == 0)) {
        RCSTAbits.CREN = 0; // Disable reception
        kernel_pushFromISR(&machine, &Event_UsartRxComplete);
    }
    else {
        buffer.bytes[0] = INVALID_ADDRESS;
    }
    rxIndex = 0;
    rxOverflowed = false;
}

void modbus_rxISR(uint8_t rxByte)
{
    TMR_COUNTER = TMR_PRELOAD;
    TMR_CLEAR_FLAG();
    TMR_EI();
    if (rxIndex < BUFFER_SIZE) {
        buffer.bytes[rxIndex++] = rxByte;
    }
    else {
        rxOverflowed = true;
    }
}

void modbus_txISR(void)
{
    DE_PIN = 1;
    TXREG = *(txPtr++);
    if (--nTxRemaining == 0) {
        PIE1bits.TXIE = 0; // Disable TX interrupt
        ENABLE_RECEIVER();
    }
}

//==============================================================================
// State Functions
//==============================================================================

static void entryState(Event* event)
{
    if (event->type != EventType_UsartRxComplete) {
        return;
    }

    // Check if the function code supported
    bool fCodeValid = false;
    switch (buffer.bytes[1]) {
    case FuncCode_ReadHolding:      case FuncCode_ReadInput:
    case FuncCode_WriteMultiple:    case FuncCode_WriteSingle:
        fCodeValid = true;
    }
    if (!fCodeValid) {
        sendException(ExCode_IllegalFunc);
        return;
    }

    // Check CRC
    if (!isCrcValid()) {
        ENABLE_RECEIVER();
        return;
    }

    // Determine next state depending on function code
    switch (buffer.bytes[1]) {
    case FuncCode_ReadHolding:
    case FuncCode_ReadInput:
        machine.stateFunction = &function_3_4;
        break;
    case FuncCode_WriteSingle:
        machine.stateFunction = &function_6;
        break;
    case FuncCode_WriteMultiple:
        machine.stateFunction = &function_16;
        break;
    }
    kernel_push(&machine, &Event_Entry);
}

static void function_3_4(Event* event)
{
    if (event->type != EventType_Entry) {
        return;
    }

    // Check for data errors in request
    uint16_t nRegs = BYTE_SWAP_16(buffer.request_3_4.nRegs_rev);
    if (nRegs == 0 || nRegs > 0x007d) {
        machine.stateFunction = &entryState;
        sendException(ExCode_IllegalDataValue);
        return;
    }

    // Find the dictionary owning the requested registers
    const Dictionary* dict;
    uint16_t index = BYTE_SWAP_16(buffer.request_3_4.index_rev);
    if (buffer.request_3_4.function == FuncCode_ReadInput) {
        dict = findInputDictionary(index, nRegs);
    }
    else {
        dict = findHoldingDictionary(index, nRegs);
    }
    if (dict == NULL) {
        machine.stateFunction = &entryState;
        sendException(ExCode_IllegalDataAdr);
        return;
    }

    // Prepare the response message
    uint8_t nBytes = (uint8_t)(nRegs * 2);
    buffer.response_3_4.nBytes = nBytes;
    Dictionary_readToBuffer(dict, index, nRegs,
        (uint8_t*)&buffer.response_3_4.firstData_rev);
    uint8_t* crcInBuf = (uint8_t*)(&buffer.response_3_4.firstData_rev + nRegs);
    uint16_t crc = crc_16(buffer.bytes, nBytes + 3);
    *crcInBuf = (uint8_t)crc;
    *(crcInBuf + 1) = crc >> 8;

    // Initiate transmission and reset the state machine
    machine.stateFunction = &entryState;
    nTxRemaining = nBytes + 5; // total transfer size, including CRC
    txPtr = buffer.bytes;
    PIE1bits.TXIE = 1;
    return;
}

static void function_6(Event* event)
{
    if (event->type != EventType_Entry) {
        return;
    }

    // Find the dictionary owning the requested registers
    uint16_t index = BYTE_SWAP_16(buffer.request_6.index_rev);
    const Dictionary* dict = findHoldingDictionary(index, 1);
    if (dict == NULL) {
        machine.stateFunction = &entryState;
        sendException(ExCode_IllegalDataAdr);
        return;
    }

    // Update the register
    Dictionary_writeFromBuffer(dict, index, 1,
        (uint8_t*)&buffer.request_6.value_rev);

    // The response is the same as the request. Nothing needs to be done.

    // Initiate transmission and reset the state machine
    machine.stateFunction = &entryState;
    nTxRemaining = 8; // total transfer size, including CRC
    txPtr = buffer.bytes;
    PIE1bits.TXIE = 1;
}

static void function_16(Event* event)
{
    if (event->type != EventType_Entry) {
        return;
    }

    // Check for data errors in request
    uint16_t nRegs = BYTE_SWAP_16(buffer.request_16.nRegs_rev);
    uint8_t nBytes = buffer.request_16.nBytes;
    if (nRegs == 0 || nRegs > 0x007b || nBytes != (uint8_t)(nRegs * 2)) {
        machine.stateFunction = &entryState;
        sendException(ExCode_IllegalDataValue);
        return;
    }

    // Find the dictionary owning the requested registers
    uint16_t index = BYTE_SWAP_16(buffer.request_16.index_rev);
    const Dictionary* dict = findHoldingDictionary(index, nRegs);
    if (dict == NULL) {
        machine.stateFunction = &entryState;
        sendException(ExCode_IllegalDataAdr);
        return;
    }

    // Update the registers
    Dictionary_writeFromBuffer(dict, index, nRegs,
        (uint8_t*)&buffer.request_16.firstData_rev);

    // Prepare the response message
    buffer.response_16.crc = crc_16(buffer.bytes, 6);

    // Initiate transmission and reset the state machine
    machine.stateFunction = &entryState;
    nTxRemaining = 8; // total transfer size, including CRC
    txPtr = buffer.bytes;
    PIE1bits.TXIE = 1;
}


//==============================================================================
// Local Helper Functions
//==============================================================================

static void sendException(ExCode code)
{
        // Prepare the exception response
        // buffer.exception.address = address; // Already in buffer
        buffer.exception.function |= 0x80;
        buffer.exception.code = code;
        buffer.exception.crc = crc_16(buffer.bytes, 3);

        // Initiate transmission
        txPtr = buffer.bytes;
        nTxRemaining = 5;
        // DE_PIN = 1;
        PIE1bits.TXIE = 1;
}

static bool Dictionary_owns(const Dictionary* dict, uint16_t regIndex,
        uint16_t nRegs)
{
    uint16_t dictStart = dict->startIndex;
    __uint24 dictEnd = (__uint24)dictStart + dict->nRegs; // exclusive
    return (regIndex >= dictStart) && ((__uint24)regIndex + nRegs <= dictEnd);
}

static void Dictionary_writeFromBuffer(const Dictionary* dict, uint16_t regIndex,
        uint16_t nRegs, const byte* buffer)
{
    uint16_t* regPtr = dict->regs + (regIndex - dict->startIndex);
    for (uint8_t i = 0; i < nRegs; ++i) {
        uint16_t temp;
        memcpy(&temp, buffer, sizeof(temp));
        temp = BYTE_SWAP_16(temp);
        memcpy(regPtr, &temp, sizeof(uint16_t));
        ++regPtr;
        buffer += 2;
    }
}

static void Dictionary_readToBuffer(const Dictionary* dict, uint16_t regIndex,
        uint16_t nRegs, byte* buffer)
{
    const uint16_t* regPtr = dict->regs + (regIndex - dict->startIndex);
    for (uint8_t i = 0; i < nRegs; ++i) {
        uint16_t temp = *regPtr;
        temp = BYTE_SWAP_16(temp);
        memcpy(buffer, &temp, sizeof(uint16_t));
        ++regPtr;
        buffer += 2;
    }
}

static const Dictionary* findInputDictionary(uint16_t regIndex, uint16_t nRegs)
{
    for (uint8_t i = 0; i < nInputDicts; ++i) {
        if (Dictionary_owns(inputDicts[i], regIndex, nRegs)) {
            return inputDicts[i];
        }
    }
    return NULL;
}

static const Dictionary* findHoldingDictionary(uint16_t regIndex, uint16_t nRegs)
{
    for (uint8_t i = 0; i < nHoldingDicts; ++i) {
        if (Dictionary_owns(holdingDicts[i], regIndex, nRegs)) {
            return holdingDicts[i];
        }
    }
    return NULL;
}


static bool isCrcValid(void)
{
    uint16_t receivedCrc;
    uint16_t calculatedCrc;

    switch (buffer.bytes[1]) {

    case FuncCode_ReadHolding:
    case FuncCode_ReadInput:
    case FuncCode_WriteSingle:
        receivedCrc = buffer.request_3_4_6.crc;
        calculatedCrc = crc_16(buffer.bytes, 6);
        break;

    case FuncCode_WriteMultiple:
    {
        uint8_t msgSize = 7 + buffer.request_16.nBytes; // excluding CRC field
        receivedCrc = (uint16_t)(buffer.bytes[msgSize]
                | buffer.bytes[msgSize + 1] << 8);
        calculatedCrc = crc_16(buffer.bytes, msgSize);
        break;
    }

    default: return false; // Execution probably never reaches here
    } // switch (FuncCode)

    return receivedCrc == calculatedCrc;
}

