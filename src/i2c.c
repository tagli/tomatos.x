/**
 * @file i2c.c
 * @author Gokce Taglioglu
 */

#include "i2c.h"
#include <xc.h>
#include "diagnostics.h"
#include "eventq.h"

//==============================================================================
// Local Definitions & Settings
//==============================================================================

#define MACHINE_PRIORITY 0
#define REQ_QUEUE_LENGTH 4 // used for jobs received during busy time

struct XferVars {
    EventType reqType;
    Machine* client;
    uint8_t slaveAdr;
    union {
        const byte* source;
        byte* target;
    };
    size_t nBytesRequested;
    size_t nBytesRemaining;
};

//==============================================================================
// Local Function Prototypes
//==============================================================================

// State Functions
static void state_idle(Event* event);
static void state_busy(Event* event);

// Other Helper Functions
static void serveRequest(Event* event);

//==============================================================================
// Local Variables
//==============================================================================

static Machine machine = {&state_idle, MACHINE_PRIORITY};

static volatile union {
    struct XferVars xfer;
} subState;

static Event* reqBuffer[REQ_QUEUE_LENGTH] = {NULL};
static EventQ reqQ = {0};

static Event Event_I2C_RespOk = STATIC_EVENT(EventType_I2C_RespOk);
static Event Event_I2C_SlaveNak = STATIC_EVENT(EventType_I2C_SlaveNak);
static Event Event_I2C_XferComplete = STATIC_EVENT(EventType_I2C_XferComplete);

enum {
    State_Idle,
    State_WaitingStart, // waiting for start condition to complete
    State_WaitingAddress, // waiting for address transfer
    State_Transmit,
    State_Receive,
    State_WaitingAck, // Waiting for completion of its own ACK sequence
    State_WaitingStop, // waiting for start condition to complete
} isrState = State_Idle;

//==============================================================================
// Global Functions
//==============================================================================

void i2c_initialize(void)
{
    TRISCbits.TRISC3 = 1;
    TRISCbits.TRISC4 = 1;
    SSPADD = 49; // 100 kHz @ 20 MHz Fosc
    SSPSTATbits.SMP = 1; // 100 kHz mode
    SSPCON1 = (0b1000u << _SSPCON1_SSPM_POSN) // Master mode
            | _SSPCON1_SSPEN_MASK; // Enable MSSP
    // Interrupts are enabled in state functions

    EventQ_create(&reqQ, reqBuffer, REQ_QUEUE_LENGTH);
    machine.stateFunction = &state_idle;
}

void i2c_request(I2cXferReq* req)
{
    if (req->size == 0 && req->client != NULL) {
        kernel_push(req->client, &Event_I2C_RespOk);
    }
    else {
        ASSERT(req->target != NULL); // or source?
        kernel_push(&machine, (Event*)req);
    }
}

//==============================================================================
// Interrupt Functions (called by ISR)
//==============================================================================

void i2c_ISR(void)
{
    switch (isrState) {

    case State_WaitingStart:
    {
        uint8_t adrRw = (uint8_t)(subState.xfer.slaveAdr << 1);
        if (subState.xfer.reqType == EventType_I2C_ReadReq) adrRw |= 1;
        SSPBUF = adrRw;
        isrState = State_WaitingAddress;
        break;
    }

    case State_WaitingAddress:
        // Check if slave sends ACK
        if (SSPCON2bits.ACKSTAT == 1) {
            SSPCON2bits.PEN = 1; // Send stop
            isrState = State_WaitingStop;
        }
        else if (subState.xfer.reqType == EventType_I2C_ReadReq) {
            SSPCON2bits.RCEN; // Start reception
            isrState = State_Receive;
        }
        else {
            SSPBUF = *subState.xfer.source++;
            isrState = State_Transmit;
        }
        break;

    case State_Transmit:
        // Check if slave sends ACK
        if (SSPCON2bits.ACKSTAT == 1) {
            SSPCON2bits.PEN = 1; // Send stop
            isrState = State_WaitingStop;
        }
        else {
            if (--subState.xfer.nBytesRemaining == 0) {
                SSPCON2bits.PEN = 1; // Send stop
                isrState = State_WaitingStop;
            }
            else {
                SSPBUF = *subState.xfer.source++;
                // No state change
            }
        }
        break;

    case State_Receive:
        *subState.xfer.target++ = SSPBUF;
        --subState.xfer.nBytesRemaining;
        SSPCON2bits.ACKDT = (subState.xfer.nBytesRemaining == 0) ? 1 : 0;
        SSPCON2bits.ACKEN = 1;
        isrState = State_WaitingAck;
        break;

    case State_WaitingAck:
        if (subState.xfer.nBytesRemaining == 0) {
            kernel_pushFromISR(&machine, &Event_I2C_XferComplete);
            PIE1bits.SSPIE = 0; // Disable interrupt
            isrState = State_Idle;
        }
        else {
            SSPCON2bits.RCEN; // Start reception
            isrState = State_Receive;
        }
        break;

    case State_WaitingStop:
        if (subState.xfer.reqType == EventType_I2C_WriteReq
                && SSPCON2bits.ACKSTAT == 1)
        {
            kernel_pushFromISR(&machine, &Event_I2C_SlaveNak);
        }
        else {
            kernel_pushFromISR(&machine, &Event_I2C_XferComplete);
        }
        PIE1bits.SSPIE = 0; // Disable interrupt
        isrState = State_Idle;
        break;

    default:
        ASSERT(false);
        break;
    } // switch isrState
}

//==============================================================================
// State Functions
//==============================================================================

static void state_idle(Event* event)
{
    switch (event->type) {
    case EventType_I2C_WriteReq:
    case EventType_I2C_ReadReq:
        serveRequest(event);
        machine.stateFunction = &state_busy;
        break;
    default:
        ASSERT(false);
        break;
    }
}

static void state_busy(Event* event)
{
    switch (event->type) {

    case EventType_I2C_WriteReq:
    case EventType_I2C_ReadReq:
    {
        Result result = EventQ_push(&reqQ, event);
        ASSERT(result == Result_Ok);
        break;
    }

    case EventType_I2C_XferComplete:
    {
        // Notify the client
        if (subState.xfer.client != NULL) {
            kernel_push(subState.xfer.client, &Event_I2C_RespOk);
        }
        // Process other requests if there are any
        Event* pendingRequest = EventQ_pop(&reqQ);
        if (pendingRequest) {
            serveRequest(pendingRequest);
            machine.stateFunction = &state_busy;
        }
        else {
            machine.stateFunction = &state_idle;
        }
        Event_releaseDestroy(pendingRequest);
        break;
    }

    case EventType_I2C_SlaveNak:
    {
        // Notify the client
        if (subState.xfer.client != NULL) {
            I2cRespErr* response = (I2cRespErr*)Event_create(
                    EventType_I2C_RespErr, sizeof(I2cRespErr));
            ASSERT(response != NULL);
            response->nBytesXfered = subState.xfer.nBytesRequested
                    - subState.xfer.nBytesRemaining;
            kernel_push(subState.xfer.client, (Event*)response);
        }
        // Process other requests if there are any
        Event* pendingRequest = EventQ_pop(&reqQ);
        if (pendingRequest) {
            serveRequest(pendingRequest);
            machine.stateFunction = &state_busy;
        }
        else {
            machine.stateFunction = &state_idle;
        }
        Event_releaseDestroy(pendingRequest);
        break;
    }
    default:
        ASSERT(false);
        break;
    } // end switch
}

//==============================================================================
// Local Helper Functions
//==============================================================================

static void serveRequest(Event* event)
{
    switch (event->type) {
    case EventType_I2C_WriteReq:
    case EventType_I2C_ReadReq:
    {
        // Copy operating parameters
        I2cXferReq* req = (I2cXferReq*)event;
        subState.xfer.reqType = req->super.type;
        subState.xfer.client = req->client;
        subState.xfer.slaveAdr = req->slaveAdr;
        if (subState.xfer.reqType == EventType_I2C_WriteReq)
            subState.xfer.source = req->source;
        else
            subState.xfer.target = req->target;
        subState.xfer.nBytesRequested = req->size;
        subState.xfer.nBytesRemaining = req->size;

        // Start xfer of the first byte
        isrState = State_WaitingStart;
        PIR1bits.SSPIF = 0;
        PIE1bits.SSPIE = 1; // Enable interrupts
        SSPCON2bits.SEN = 1; // Start condition
        break;
    }
    default:
        ASSERT(false);
        break;
    } // end switch
}

