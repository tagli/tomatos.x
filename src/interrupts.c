/**
 * @file interrupts.c
 * @author Gokce Taglioglu
 */

#include <xc.h>
#include "event_types.h"
#include "wakeup.h"
#include "modbus.h"
#include "eeprom2.h"
#include "i2c.h"

static void __interrupt() isr(void)
{
    // TMR2 is used for periodic sysTick updates
    if (PIE1bits.TMR2IE && PIR1bits.TMR2IF) {
        wakeup_tickFromISR();
        PIR1bits.TMR2IF = 0;
    }

    // TMR0 is used for Modbus RX idle line detection
    if (INTCONbits.T0IE && INTCONbits.T0IF) {
        modbus_timeoutISR();
        INTCONbits.T0IF = 0;
    }

    // USART RX is used for Modbus data reception
    if (PIE1bits.RCIE && PIR1bits.RC1IF) {
        modbus_rxISR(RCREG);
    }

    // USART TX is used for Modbus data transmission
    if (PIE1bits.TXIE && PIR1bits.TXIF) {
        modbus_txISR();
    }

    // I2C
    if (PIE1bits.SSPIE && PIR1bits.SSPIF) {
        i2c_ISR();
        PIR1bits.SSPIF = 0;
    }

    // EEPROM Write Complete
    if (PIE2bits.EEIE && PIR2bits.EEIF) {
        eeprom2_writeCompleteISR();
        PIR2bits.EEIF = 0;
    }
}

