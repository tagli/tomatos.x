/**
 * @file flash_keys.h
 * @author Gokce Taglioglu
 */

#ifndef FLASH_KEYS_H
#define	FLASH_KEYS_H

#include <stdint.h>

enum FlashKey {
    FlashKey_Invalid,
    FlashKey_nKeys
};

typedef uint8_t rec_key_t;
_Static_assert(FlashKey_nKeys <= 256, "rec_key_t can't hold all keys values.");

#endif /* FLASH_KEYS_H */

