/**
 * @file diagnostics.h
 * @author Gokce Taglioglu
 */

#ifndef DIAGNOSTICS_H
#define	DIAGNOSTICS_H

#include <assert.h>

//==============================================================================
// Macros
//==============================================================================

#if defined(NDEBUG)
#define ASSERT(expr) ((void)0)
#elif !defined(__DEBUG)
#define ASSERT(expr_) (expr_) ? (void)0 : panic()
#else
#define ASSERT(expr_) __conditional_software_breakpoint(expr_)
#endif


//==============================================================================
// Global Functions
//==============================================================================

void panic(void);

#endif /* DIAGNOSTICS_H */

