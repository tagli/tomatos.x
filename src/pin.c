/**
 * @file pin.c
 * @author Gokce Taglioglu
 */

#include "pin.h"
#include <xc.h>

//==============================================================================
// Local Variables
//==============================================================================

static volatile unsigned char* const lats[] = {&LATA, &LATB, &LATC};
static volatile unsigned char* const ports[] = {&PORTA, &PORTB, &PORTC};

//==============================================================================
// Global Functions
//==============================================================================

void pin_set(Pin p)
{
    *lats[p.port] |= (1 << p.no);
}

void pin_reset(Pin p)
{
    *lats[p.port] &= ~(1 << p.no);
}

bool pin_test(Pin p)
{
    return *ports[p.port] & (1 << p.no);
}

