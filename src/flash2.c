/**
 * @file flash2.c
 * @author Gokce Taglioglu
 */

#include "flash2.h"
#include <xc.h>
#include <stdbool.h>
#include <string.h>
#include "common_defs.h"

//==============================================================================
// Definitions & Settings
//==============================================================================

#define MASK ((flash_adr_t)(_FLASH_WRITE_SIZE - 1)) // Ex: 8 -> 0b1111

//==============================================================================
// Local Function Prototypes
//==============================================================================

bool isWriteBufferEmpty(const byte* buffer);
void burnBufferToFlash(flash_adr_t blockAdr, const byte* buffer);

//==============================================================================
// Global Functions
//==============================================================================

void flash2_eraseBlock(flash_adr_t blockAdr)
{
    EECON1bits.EEPGD = 1; // Access program memory
    EECON1bits.CFGS = 0; // Don't access config memory
    EECON1bits.WREN = 1; // Enable write
    EECON1bits.FREE = 1; // Enable erase

    TBLPTRL = (uint8_t)blockAdr;
    TBLPTRH = (uint8_t)(blockAdr >> 8);
    #if _ROMSIZE > 65536
    TBLPTRU = (uint8_t)(blockAdr >> 16);
    #endif

    di();
    EECON2 = 0x55;
    EECON2 = 0xaa;
    EECON1bits.WR = 1;
    ei();

    EECON1 = 0;
}

void flash2_write(flash_adr_t targetAdr, const void* source, size_t size)
{
    EECON1bits.EEPGD = 1; // Access program memory
    EECON1bits.CFGS = 0; // Don't access config memory
    EECON1bits.WREN = 1; // Enable write
    EECON1bits.FREE = 0; // Disable erase

    const byte* srcPtr = (const byte*)source;
    size_t byteCounter = 0;
    size_t remaining = size;
    byte buffer[_FLASH_WRITE_SIZE];

    while (byteCounter < size) {
        size_t possibleSize = _FLASH_WRITE_SIZE - (targetAdr & MASK);
        size_t writeSize = (possibleSize < remaining) ? possibleSize : remaining;
        memset(buffer, FLASH_EMPTY_CELL, sizeof(buffer));
        memcpy(&buffer[targetAdr & MASK], srcPtr, writeSize);
        if (!isWriteBufferEmpty(buffer)) {
            burnBufferToFlash(targetAdr & ~MASK, buffer);
        }
        byteCounter += writeSize;
        remaining -= writeSize;
        srcPtr += writeSize;
        targetAdr += writeSize;
    }
    EECON1 = 0;
}

void flash2_read(flash_adr_t sourceAdr, void* target, size_t bufSize)
{
    byte* targetPtr = (byte*)target;

    TBLPTRL = (uint8_t)sourceAdr;
    TBLPTRH = (uint8_t)(sourceAdr >> 8);
    #if _ROMSIZE > 65536
    TBLPTRU = (uint8_t)(sourceAdr >> 16);
    #endif

    while (bufSize--) {
        asm("TBLRD*+");
        *targetPtr++ = TABLAT;
    }
}

//==============================================================================
// Local Functions
//==============================================================================

bool isWriteBufferEmpty(const byte* buffer)
{
    for (uint8_t i = 0; i < _FLASH_WRITE_SIZE; ++i) {
        if (*buffer++ != FLASH_EMPTY_CELL) return false;
    }
    return true;
}

void burnBufferToFlash(flash_adr_t blockAdr, const byte* buffer)
{
        TBLPTRL = (uint8_t)blockAdr;
        TBLPTRH = (uint8_t)(blockAdr >> 8);
        #if _ROMSIZE > 65536
        TBLPTRU = (uint8_t)(blockAdr >> 16);
        #endif

        for (uint8_t i = 0; i < _FLASH_WRITE_SIZE; ++i) {
            TABLAT = *buffer++;
            asm("TBLWT*+");
        }

        asm("TBLRD*-"); // TBLPTR--; otherwise TBLPTR points to next write block
        di();
        EECON2 = 0x55;
        EECON2 = 0xaa;
        EECON1bits.WR = 1;
        ei();
}

