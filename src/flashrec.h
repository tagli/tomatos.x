/**
 * @file flashrec.h
 * @author Gokce Taglioglu
 */

#ifndef FLASHREC_H
#define	FLASHREC_H

#include <stdint.h>
#include <stddef.h>
#include <stdbool.h>
#include "flash_keys.h"

//==============================================================================
// Definitions
//==============================================================================

typedef uint8_t rec_size_t; // We assume that records are max 65535 bytes long

//==============================================================================
// Global Functions
//==============================================================================

void flashrec_mount(void);
rec_size_t flashrec_write(rec_key_t key, const void* source, rec_size_t size);
rec_size_t flashrec_read(rec_key_t key, void* target, size_t maxSize);
rec_size_t flashrec_size(rec_key_t key);
size_t flashrec_memUsed(void);
size_t flashrec_memTotal(void);

//==============================================================================
// Macros
//==============================================================================

#define flashrec_delete(key_) flashrec_write((key_), 0, 0)

#endif // FLASHREC_H

