/**
 * @file wakeup.c
 * @author Gokce Taglioglu
 */

#include "wakeup.h"
#include <xc.h>
#include <string.h>
#include "event.h"
#include "diagnostics.h"

//==============================================================================
// Local Definitions & Settings
//==============================================================================

#define MACHINE_PRIORITY 0
#define MAX_ORDERS 8
#define TMR_POSTSCALER 10

struct WakeupOrder {
    Machine* client;
    uint16_t wakeTick;
};
typedef struct WakeupOrder WakeupOrder;

//==============================================================================
// Local Function Prototypes
//==============================================================================

static void wakeupTask(Event* event);

//==============================================================================
// Local Variables
//==============================================================================

static uint16_t sysTick = 0;
static WakeupOrder orderList[MAX_ORDERS] = {0};
static Machine machine = {&wakeupTask, MACHINE_PRIORITY};

//==============================================================================
// Global Functions
//==============================================================================

void wakeup_initialize(void)
{
    // TMR2 is configured for 0.1 ms period & 1 kHz interrupts @ 20 MHz.
    // A dedicated soft prescaler is used in ISR for 100 Hz notifications.
    PR2 = 125 - 1;
    T2CON = ((10 - 1) << _T2CON_TOUTPS0_POSN) // Postscaler 1:10
            | (0b01 << _T2CON_T2CKPS0_POSN) // Prescaler 1:4
            | _T2CON_TMR2ON_MASK; // Enable TMR2
    PIE1bits.TMR2IE = 1; // Enable TMR2 interrupt
}

void wakeup_tickFromISR(void)
{
    static Event Event_SysTick = STATIC_EVENT(EventType_SysTick);
    static uint8_t counter = TMR_POSTSCALER;
    if (--counter == 0) {
        kernel_pushFromISR(&machine, &Event_SysTick);
        counter = TMR_POSTSCALER;
    }
}

Result wakeup_request(Machine* client, uint16_t tick)
{
    // Scan job queue and find an empty spot
    for (uint8_t i = 0; i < MAX_ORDERS; ++i) {
        if (orderList[i].client == NULL) {
            orderList[i].client = client;
            orderList[i].wakeTick = sysTick + tick;
            return Result_Ok;
        }
    }
    return Result_Err; // Unable to queue, no empty spot
}

//==============================================================================
// State Functions
//==============================================================================

static void wakeupTask(Event* event)
{
    ASSERT(event->type == EventType_SysTick);
    static Event Event_Wakeup = STATIC_EVENT(EventType_Wakeup);

    ++sysTick;
    for (uint8_t i = 0; i < MAX_ORDERS; ++i) {
        if (orderList[i].client != NULL && orderList[i].wakeTick == sysTick) {
            kernel_push(orderList[i].client, &Event_Wakeup);
            orderList[i].client = NULL;
        }
    }
}

