/**
 * @file crc.h
 * @author Gokce Taglioglu
 */

#ifndef CRC_H
#define	CRC_H

#include <stdint.h>
#include <stddef.h>

//==============================================================================
// Global Functions
//==============================================================================

uint16_t crc_16(const void* data, size_t length);

#endif	/* CRC_H */

