/**
 * @file ssd1306_txt.h
 * @author Gokce Taglioglu
 */

#ifndef SSD1306_TXT_H
#define	SSD1306_TXT_H

#include <stdint.h>

//==============================================================================
// Global Functions
//==============================================================================

void ssd1306_initialize(void);
void ssd1306_printStr(uint8_t row, uint8_t column, const char* str);
void ssd1306_printRaw(uint8_t row, uint8_t column, const uint8_t* data);
void ssd1306_clearAll(void);

#endif	/* SSD1306_TXT_H */

