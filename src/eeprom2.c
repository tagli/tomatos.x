/**
 * @file eeprom2.c
 * @author Gokce Taglioglu
 */

#include <xc.h>
#include "diagnostics.h"
#include "eeprom2.h"
#include "event.h"
#include "eventq.h"

//==============================================================================
// Local Definitions & Settings
//==============================================================================

#define MACHINE_PRIORITY 0
#define JOB_QUEUE_LENGTH 4 // used for jobs received during busy time

//==============================================================================
// Local Function Prototypes
//==============================================================================

// State Functions
static void state_idle(Event* event);
static void state_writeInProgress(Event* event);
static void state_processPendingReqs(Event* event);

static void writeFirstByte(void);

//==============================================================================
// Local Variables
//==============================================================================

static Machine machine = {&state_idle, MACHINE_PRIORITY};
static volatile EprWriteReq currentReq = {0};
static Event* jobBuffer[JOB_QUEUE_LENGTH] = {NULL};
static EventQ pendingReqs = {0};
static Event Event_EepromRequestComplete = STATIC_EVENT(EventType_EepromRequestComplete);

//==============================================================================
// Global Functions
//==============================================================================

void eeprom2_initialize(void)
{
    EventQ_create(&pendingReqs, jobBuffer, JOB_QUEUE_LENGTH);
    machine.stateFunction = &state_idle;
}

void eeprom2_request(struct EepromRequest* req)
{
    if (req->size == 0 && req->client != NULL) {
        kernel_push(req->client, &Event_EepromRequestComplete);
    }
    else {
        ASSERT(req->buffer != NULL);
        //ASSERT(job->adr < EEPROM_SIZE);
        kernel_push(&machine, (Event*)req);
    }
}

void eeprom2_read_raw(epr_adr_t adr, void* target, epr_size_t size)
{
    ASSERT(target != NULL);
    //ASSERT(adr < EEPROM_SIZE);
    byte* targetPtr = (byte*)target;
    EECON1bits.EEPGD = 0; // Access EEPROM
    EECON1bits.CFGS = 0;

    while (size-- != 0) {
        EEADR = adr++;
        EECON1bits.RD = 1; // Start read operation
        *targetPtr++ = EEDATA;
    }
}

void eeprom2_write_raw(epr_adr_t adr, const void* source, epr_size_t size)
{
    ASSERT(source != NULL);
    //ASSERT(adr < EEPROM_SIZE);
    const byte* srcPtr = (const byte*)source;
    EECON1bits.EEPGD = 0; // Access to EEPROM
    EECON1bits.CFGS = 0; // Access to EEPROM
    EECON1bits.WREN = 1; // Enable write operations

    while (size-- != 0) {
        EEADR = adr++;
        EEDATA = *srcPtr++;
        di();
        EECON2 = 0x55;
        EECON2 = 0xaa;
        EECON1bits.WR = 1;
        ei();
        while (EECON1bits.WR == 1) {} // wait write operation
    }
    EECON1bits.WREN = 0; // Disable write operations
}

//==============================================================================
// Interrupt Functions (called by ISR)
//==============================================================================

void eeprom2_writeCompleteISR(void)
{
    static Event Event_EepromWriteComplete = STATIC_EVENT(EventType_EepromWriteComplete);
    if (--currentReq.size != 0) {
        EEADR = currentReq.adr++;
        EEDATA = *currentReq.buffer++;
        EECON1bits.EEPGD = 0; // Access to EEPROM
        EECON1bits.CFGS = 0; // Access to EEPROM
        EECON1bits.WREN = 1; // Enable write operations
        EECON2 = 0x55;
        EECON2 = 0xaa;
        EECON1bits.WR = 1;
    }
    else {
        PIE2bits.EEIE = 0; // Disable EEPROM interrupts
        EECON1bits.WREN = 0; // Disable write operations
        kernel_pushFromISR(&machine, &Event_EepromWriteComplete);
    }
}

//==============================================================================
// State Functions
//==============================================================================

static void state_idle(Event* event)
{
    if (event->type == EventType_EepromWriteRequest) {
        currentReq = *((EprWriteReq*)event); // copy
        machine.stateFunction = &state_writeInProgress;
        writeFirstByte();
    }
    else if (event->type == EventType_EepromReadRequest) {
        EprReadReq* req = (EprReadReq*)event;
        eeprom2_read_raw(req->adr, req->buffer, req->size);
        if (req->client != NULL) {
            kernel_push(req->client, &Event_EepromRequestComplete);
        }
    }
    else {
        ASSERT(false);
    }
}

static void state_processPendingReqs(Event* event)
{
    // We are busy, add new request to pending request queue
    if (event->type == EventType_EepromWriteRequest
            || event->type == EventType_EepromReadRequest)
    {
        bool result = EventQ_push(&pendingReqs, event);
        ASSERT(result == Result_Ok);
    }
    // Handle pending requests
    else if (event->type == EventType_Entry) {
        Event* req = EventQ_pop(&pendingReqs);
        if (req->type == EventType_EepromWriteRequest) {
            currentReq = *((EprWriteReq*)req); // copy
            machine.stateFunction = &state_writeInProgress;
            writeFirstByte();
        }
        else { // EventType_EepromReadRequest
            EprReadReq* req = (EprReadReq*)event;
            eeprom2_read_raw(req->adr, req->buffer, req->size);
            if (req->client != NULL) {
                kernel_push(req->client, &Event_EepromRequestComplete);
            }
            if (EventQ_isEmpty(&pendingReqs)) {
                machine.stateFunction = &state_idle;
            }
            else {
                // Remain in state_processPendingReqs
                kernel_push(&machine, &Event_Entry);
            }
        }
        Event_releaseDestroy(req);
    }
    else {
        ASSERT(false);
    }
}

static void state_writeInProgress(Event* event)
{
    // We are busy, add new job to pending job queue
    if (event->type == EventType_EepromWriteRequest) {
        bool result = EventQ_push(&pendingReqs, event);
        ASSERT(result == Result_Ok);
    }
    // ISR reports that all bytes are written
    else if (event->type == EventType_EepromWriteComplete) {
        if (currentReq.client != NULL) {
            kernel_push(currentReq.client, &Event_EepromRequestComplete);
        }
        if (EventQ_isEmpty(&pendingReqs)) {
            machine.stateFunction = &state_idle;
        }
        else {
            machine.stateFunction = &state_processPendingReqs;
            kernel_push(&machine, &Event_Entry);
        }
    }
    else {
        ASSERT(false);
    }
}

//==============================================================================
// Local Helper Functions
//==============================================================================

static void writeFirstByte(void)
{
    EEADR = currentReq.adr++;
    EEDATA = *currentReq.buffer++;
    EECON1bits.EEPGD = 0; // Access to EEPROM
    EECON1bits.CFGS = 0; // Access to EEPROM
    EECON1bits.WREN = 1; // Enable write operations
    di();
    EECON2 = 0x55;
    EECON2 = 0xaa;
    EECON1bits.WR = 1;
    ei();
    PIR2bits.EEIF = 0; // Clear flag
    PIE2bits.EEIE = 1; // Enable EEPROM interrupt
}

