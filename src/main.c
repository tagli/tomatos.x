/**
 * @file main.c
 * @author Gokce Taglioglu
 */

#include "config_bits.h"
#include <xc.h>
#include <stdbool.h>
#include "common_defs.h"
#include "diagnostics.h"
#include "kernel.h"
#include "flashrec.h"
#include "wakeup.h"
#include "blinker.h"
#include "modbus.h"
#include "nvsimple.h"
#include "ssd1306_txt.h"

//==============================================================================
// Local Function Prototypes
//==============================================================================

static void idleTask(void);

//==============================================================================
// Local Variables
//==============================================================================

//==============================================================================
// Main
//==============================================================================

int main(void) {

    kernel_initialize();
    wakeup_initialize();
    modbus_initialize();
    blinker_initialize();
    flashrec_mount();
    nvsimple_initialize();
    ssd1306_initialize();

    // Welcome Screen
    ssd1306_printStr(1, 4, "<< tomatOS >>");
    ssd1306_printStr(3, 10, "by");
    ssd1306_printStr(5, 3, "Gokce Taglioglu");

    INTCONbits.PEIE = 1;
    ei();

    while (true) {
        Machine* machine;
        Event* event;
        Result result = kernel_pop(&machine, &event);
        if (result == Result_Some) {
            machine->stateFunction(event);
            Event_releaseDestroy(event);
        }
        else {
            idleTask();
        }
    }
}

//==============================================================================
// Local Functions
//==============================================================================

static void idleTask(void) {

}

