/**
 * @file pin.h
 * @author Gokce Taglioglu
 */

#ifndef PIN_H
#define	PIN_H

#include <stdint.h>
#include <stdbool.h>

//==============================================================================
// Definitions
//==============================================================================

enum Port {
    Port_A = 0, Port_B = 1, Port_C = 2
};

struct Pin {
    uint8_t port : 4;
    uint8_t no : 4;
};
typedef struct Pin Pin;

//==============================================================================
// Global Functions
//==============================================================================

void pin_set(Pin p);
void pin_reset(Pin p);
bool pin_test(Pin p);

//==============================================================================
// Pin Definitions for PIC18F252
//==============================================================================

#define PIN_A0 (Pin){.port = Port_A, .no = 0}
#define PIN_A1 (Pin){.port = Port_A, .no = 1}
#define PIN_A2 (Pin){.port = Port_A, .no = 2}
#define PIN_A3 (Pin){.port = Port_A, .no = 3}
#define PIN_A4 (Pin){.port = Port_A, .no = 4}
#define PIN_A5 (Pin){.port = Port_A, .no = 5}
#define PIN_A6 (Pin){.port = Port_A, .no = 6}

#define PIN_B0 (Pin){.port = Port_B, .no = 0}
#define PIN_B1 (Pin){.port = Port_B, .no = 1}
#define PIN_B2 (Pin){.port = Port_B, .no = 2}
#define PIN_B3 (Pin){.port = Port_B, .no = 3}
#define PIN_B4 (Pin){.port = Port_B, .no = 4}
#define PIN_B5 (Pin){.port = Port_B, .no = 5}
#define PIN_B6 (Pin){.port = Port_B, .no = 6}
#define PIN_B7 (Pin){.port = Port_B, .no = 7}

#define PIN_C0 (Pin){.port = Port_C, .no = 0}
#define PIN_C1 (Pin){.port = Port_C, .no = 1}
#define PIN_C2 (Pin){.port = Port_C, .no = 2}
#define PIN_C3 (Pin){.port = Port_C, .no = 3}
#define PIN_C4 (Pin){.port = Port_C, .no = 4}
#define PIN_C5 (Pin){.port = Port_C, .no = 5}
#define PIN_C6 (Pin){.port = Port_C, .no = 6}
#define PIN_C7 (Pin){.port = Port_C, .no = 7}

#endif	/* PIN_H */

