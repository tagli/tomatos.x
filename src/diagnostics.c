/**
 * @file diagnostics.c
 * @author Gokce Taglioglu
 */

#include <xc.h>
#include <stdbool.h>
#include "diagnostics.h"

//==============================================================================
// Global Functions
//==============================================================================

void panic(void)
{
    di();
    // TODO: Perform safe shutdown
    TRISCbits.TRISC0 = 0; // for panic led
    LATCbits.LATC0 = 1; // Panic LED
    __debug_break();
    while (true) {
        NOP();
    }
}

