/**
 * @file event.c
 * @author Gokce Taglioglu
 */

#include "event.h"
#include <stddef.h>
#include <string.h>
#include "common_defs.h"
#include "diagnostics.h"
#include "queue.h"

//==============================================================================
// Local Definitions & Settings
//==============================================================================

#define N_POOLS 2

#define N_SMALL_BLOCKS 8
#define N_LARGE_BLOCKS 4

#define SMALL_BLOCK_SIZE 8
#define LARGE_BLOCK_SIZE 16

//==============================================================================
// Local Function Prototypes
//==============================================================================

static uint8_t poolIndexOf(byte* memory);

//==============================================================================
// Local Variables
//==============================================================================

static uint8_t BlockSizes[N_POOLS] = {
    SMALL_BLOCK_SIZE, LARGE_BLOCK_SIZE,
};

static byte pool0Memory[N_SMALL_BLOCKS * SMALL_BLOCK_SIZE];
static byte* pool0MemoryEnd = pool0Memory + (N_SMALL_BLOCKS * SMALL_BLOCK_SIZE);
static byte pool1Memory[N_LARGE_BLOCKS * LARGE_BLOCK_SIZE];
static byte* poo1MemoryEnd = pool1Memory + (N_LARGE_BLOCKS * LARGE_BLOCK_SIZE);

static Event* pool0ListMemory[N_SMALL_BLOCKS] = {NULL};
static Event* pool1ListMemory[N_LARGE_BLOCKS] = {NULL};

static Queue freeLists[N_POOLS] = {0};

//==============================================================================
// Global Variables
//==============================================================================

Event Event_None = STATIC_EVENT(EventType_None);
Event Event_Entry = STATIC_EVENT(EventType_Entry);
Event Event_Reset = STATIC_EVENT(EventType_Reset);
Event Event_Continue = STATIC_EVENT(EventType_Continue);

//==============================================================================
// Global Functions
//==============================================================================

void Event_initialize(void)
{
    memset(pool0Memory, 0, sizeof(pool0Memory));
    memset(pool1Memory, 0, sizeof(pool1Memory));

    Queue_create(&freeLists[0], (byte*)pool0ListMemory, sizeof(pool0ListMemory[0]), N_SMALL_BLOCKS);
    Queue_create(&freeLists[1], (byte*)pool1ListMemory, sizeof(pool1ListMemory[0]), N_LARGE_BLOCKS);

    for (uint8_t i = 0; i < N_SMALL_BLOCKS; ++i) {
        Event* freeSlot = (Event*)(pool0Memory + (i * SMALL_BLOCK_SIZE));
        Queue_send(&freeLists[0], &freeSlot);
    }

    for (uint8_t i = 0; i < N_LARGE_BLOCKS; ++i) {
        Event* freeSlot = (Event*)(pool1Memory + (i * LARGE_BLOCK_SIZE));
        Queue_send(&freeLists[1], &freeSlot);
    }
}

void Event_hold(Event* event)
{
    ASSERT(event->refs != 0);
    if (event->refs > 0) {
        ASSERT(event->refs != INT8_MAX);
        ++(event->refs);
    }
}

Event* Event_create(EventType type, uint8_t size)
{
    ASSERT(size > sizeof(Event) && size <= LARGE_BLOCK_SIZE);
    Event* freePtr = NULL;
    Queue* freeList = NULL;
    Result result = Result_None;

    // Find the pool with the correct size
    for (uint8_t i = 0; i < N_POOLS; ++i) {
        if (size <= BlockSizes[i]) {
            freeList = &freeLists[i];
            break;
        }
    }
    ASSERT(freeList);

    result = Queue_receive(freeList, &freePtr);
    if (result == Result_Some) {
        freePtr->type = type;
        freePtr->refs = 1;
    }

    return freePtr;
}

void Event_releaseDestroy(Event* event)
{
    // Check if it's a dynamic event
    if (event == NULL || event->refs < 0) {
        return;
    }

    // Check if there are other references
    ASSERT(event->refs != 0);
    --event->refs;
    if (event->refs != 0) {
        return;
    }

    // Return memory address to the correct free list
    Queue* pool = &freeLists[poolIndexOf((byte*)event)];
    Queue_send(pool, &event);
}

//==============================================================================
// Local Functions
//==============================================================================

static uint8_t poolIndexOf(byte* memory)
{
    if (memory >= pool0Memory && memory < pool0MemoryEnd) {
        return 0;
    }
    else {
        return 1;
    }
}

