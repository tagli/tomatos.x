/**
 * @file blinker.c
 * @author Gokce Taglioglu
 */

#include "blinker.h"
#include <xc.h>
#include "kernel.h"
#include "diagnostics.h"
#include "wakeup.h"
#include "modbus.h"
#include "ssd1306_txt.h"

//==============================================================================
// Definitions & Settings
//==============================================================================

#define MACHINE_PRIORITY 0
#define LED LATBbits.LATB0
#define LED_TRIS TRISBbits.TRISB0

struct RegDefs{
    uint16_t onTime;
    uint16_t offTime;
};
typedef struct RegDefs RegDefs;

//==============================================================================
// Local Function Prototypes
//==============================================================================

static void blinkerTask(Event* event);

//==============================================================================
// Local Variables
//==============================================================================

static Machine machine = {&blinkerTask, MACHINE_PRIORITY};
static RegDefs hRegs = {.onTime = 20, .offTime = 80};

static const Dictionary hRegs_dict = {
    .startIndex = 100,
    .nRegs = sizeof(hRegs) / sizeof(uint16_t),
    .regs = (uint16_t*)&hRegs,
    .owner = &machine
};

enum {LED_ON, LED_OFF} ledState = LED_OFF;

//==============================================================================
// Global Functions
//==============================================================================

void blinker_initialize(void)
{
    LED_TRIS = 0; // LED output
    modbus_addHoldingDictionary(&hRegs_dict);
    wakeup_request(&machine, hRegs.offTime);
}

//==============================================================================
// State Functions
//==============================================================================

static void blinkerTask(Event* event)
{
    if (event->type == EventType_Wakeup) {
        if (ledState == LED_OFF) {
            LED = 1;
            ledState = LED_ON;
            wakeup_request(&machine, hRegs.onTime);
        }
        else {
            LED = 0;
            ledState = LED_OFF;
            wakeup_request(&machine, hRegs.offTime);
        }
    }
    else {
        ASSERT(false);
    }
}

