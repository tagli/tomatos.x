/**
 * @file flashrec.h
 * @author Gokce Taglioglu
 */

#include "flashrec.h"
#include <string.h>
#include "common_defs.h"
#include "flash2.h"
#include "crc.h"

//==============================================================================
// Definitions & Settings
//==============================================================================

typedef uint16_t rec_crc_t; // Probably CRC-16 will be used
#define BLOCK_SIZE 16
#define BLOCK_PAYLOAD_SIZE 11
#define SECTOR_SIZE 1024
#define N_BLOCKS (SECTOR_SIZE / BLOCK_SIZE)
#define STORE_START_ADR (_ROMSIZE - (2 * SECTOR_SIZE)) /* 0x7800 */

#if BLOCK_SIZE % _FLASH_WRITE_SIZE != 0
#warning "It's better if block size is multiple of flash write size."
#endif
#if STORE_START_ADR % BLOCK_SIZE != 0
#error "Write blocks are unaligned with Sector 0."
#endif

#if SECTOR_SIZE % _FLASH_ERASE_SIZE != 0
#error "Sector size must be multiple of flash erase size."
#endif
#if SECTOR_SIZE % BLOCK_SIZE != 0
#error "Sector size must be multiple of block size."
#endif
#if STORE_START_ADR % _FLASH_ERASE_SIZE != 0
#error "Erase blocks are unaligned with Sector 0."
#endif

#if N_BLOCKS < 255
typedef uint8_t blk_idx_t;
#elif N_BLOCKS < 32767
typedef int16_t blk_idx_t;
#else
#error "N_BLOCKS must fit into int16_t."
#endif

struct RecordHeader {
    uint8_t cycle;
    rec_key_t key;
    rec_size_t size;
};
typedef struct RecordHeader RecordHeader;

struct RecordSegment {
    RecordHeader header;
    byte data[BLOCK_PAYLOAD_SIZE];
};
typedef struct RecordSegment RecordSegment;

struct SectorHeader {
    uint32_t cycle;
};
typedef struct SectorHeader SectorHeader;

struct Block {
    union {
        RecordSegment recSeg;
        SectorHeader secHdr;
    } data;
    rec_crc_t crc;
};
typedef struct Block Block;

_Static_assert(sizeof(Block) == BLOCK_SIZE, "Block size is wrong.");

enum BlockStatus {
    BlockStatus_Clean,
    BlockStatus_Corrupted,
    BlockStatus_Old,
    BlockStatus_Valid,
};

//==============================================================================
// Macros
//==============================================================================

#define Block_readFromFlash(block_, flashAdr_) \
    flash2_read((flashAdr_), (block_), sizeof(Block))

#define calculate_crc(data_, size_) crc_16((data_), (size_))

//==============================================================================
// Local Function Prototypes
//==============================================================================

static int8_t findActiveSector(void);
static blk_idx_t bytesToBlocks(rec_size_t size);
static void startMigration(rec_key_t skip);
static void writeSectorHeader(flash_adr_t secAdr);
static uint32_t readSectorCycle(flash_adr_t secAdr);
static blk_idx_t calculateBlockUsage(void);

static bool Block_isCrcOk(const Block* block);
static bool Block_isClean(const Block* block, flash_adr_t adr);
static enum BlockStatus Block_evaluate(const Block* block, flash_adr_t adr);
static void Block_writeToFlash(const Block* block, flash_adr_t adr);

static rec_size_t getRecordSize(flash_adr_t recAdr);
static void writeRecord(flash_adr_t recAdr, rec_key_t key, const void* source,
        rec_size_t size);
static rec_size_t readRecord(flash_adr_t recAdr, void* target, size_t bufferSize);
static void migrateRecord(flash_adr_t srcAdr, flash_adr_t targetAdr);

//==============================================================================
// Local Variables
//==============================================================================

static const flash_adr_t SectorAdrs[2] = {
    STORE_START_ADR, STORE_START_ADR + SECTOR_SIZE,
};
static flash_adr_t cleanEndAdr = 0;
static int8_t sectorIdx = 0; // Index of the currently active sector (0 or 1)
static blk_idx_t nextBlockIdx = 0; // Index of next free block in active sector
static uint32_t cycle = 0;
static blk_idx_t jumpTable[FlashKey_nKeys] = {0};
static blk_idx_t blockUsage = 0;

//==============================================================================
// Global Functions
//==============================================================================

void flashrec_mount(void)
{
    sectorIdx = findActiveSector();
    // If no sector is valid, store is either empty or unrecoverable
    if (sectorIdx < 0) {
        nextBlockIdx = 1;
        sectorIdx = 0;
        cycle = 1;
        cleanEndAdr = STORE_START_ADR;
        writeSectorHeader(SectorAdrs[0]);
        return;
    }

    cycle = readSectorCycle(SectorAdrs[sectorIdx]);

    enum {State_Header, State_Body} state = State_Header;
    flash_adr_t blockAdr = SectorAdrs[sectorIdx] + BLOCK_SIZE;
    blk_idx_t blockIdx = 1;
    blk_idx_t recordStartIdx = 1;
    blk_idx_t nBlocksInRecord = 1;

    while (blockIdx < N_BLOCKS) {
        Block buffer;
        Block_readFromFlash(&buffer, blockAdr);
        enum BlockStatus blockStatus = Block_evaluate(&buffer, blockAdr);

        if (state == State_Header) {
            if (blockStatus == BlockStatus_Clean || blockStatus == BlockStatus_Old) {
                nextBlockIdx = blockIdx;
                break;
            }
            else if (blockStatus == BlockStatus_Valid) {
                nBlocksInRecord = bytesToBlocks(buffer.data.recSeg.header.size);
                if (nBlocksInRecord == 1) {
                    jumpTable[buffer.data.recSeg.header.key] = blockIdx;
                }
                else {
                    recordStartIdx = blockIdx;
                    --nBlocksInRecord;
                    state = State_Body;
                }
            }
            // BlockStatus_Corrupted -> continue
        }
        else { // state == State_Body (for records longer than 1 block)
            if (blockStatus == BlockStatus_Clean || blockStatus == BlockStatus_Old) {
                nextBlockIdx = recordStartIdx;
                break;
            }
            else if (blockStatus == BlockStatus_Valid) {
                --nBlocksInRecord;
                if (nBlocksInRecord == 0) {
                    jumpTable[buffer.data.recSeg.header.key] = recordStartIdx;
                    state = State_Header;
                }
            }
            else { // blockStatus == BlockStatus_Corrupted
                state = State_Header;
            }
        }
        ++blockIdx;
        blockAdr += BLOCK_SIZE;
    } // end of block loop
    // blockIdx == N_BLOCKS means sector is full

    if (blockAdr % _FLASH_ERASE_SIZE == 0) {
        cleanEndAdr = blockAdr;
    }
    else {
        cleanEndAdr = (blockAdr / _FLASH_ERASE_SIZE + 1) * _FLASH_ERASE_SIZE;
    }

    blockUsage = calculateBlockUsage();
}

rec_size_t flashrec_write(rec_key_t key, const void* source, rec_size_t size)
{
    // Ignore zero sized writes if the record does not exist or is already deleted.
    if (size == 0 && flashrec_size(key) == 0) return 0;

    // Check if there is enough logical free blocks
    int16_t removed = bytesToBlocks(flashrec_size(key));
    int16_t added = bytesToBlocks(size);
    int16_t blockDiff = added - removed;
    if (blockUsage + blockDiff > (int16_t)(N_BLOCKS - 1)) return 0;

    size_t blocksNeeded = bytesToBlocks(size);
    if (nextBlockIdx + blocksNeeded > N_BLOCKS) {
        startMigration(key);
        flash_adr_t blockAdr = SectorAdrs[sectorIdx] + (nextBlockIdx * BLOCK_SIZE);
        writeRecord(blockAdr, key, source, size);
        writeSectorHeader(SectorAdrs[sectorIdx]);
    }
    else {
        flash_adr_t blockAdr = SectorAdrs[sectorIdx] + (nextBlockIdx * BLOCK_SIZE);
        writeRecord(blockAdr, key, source, size);
    }
    jumpTable[key] = nextBlockIdx;
    nextBlockIdx += blocksNeeded;
    blockUsage = (blk_idx_t)(blockUsage + blockDiff);
    return size;
}

rec_size_t flashrec_read(rec_key_t key, void* target, size_t bufferSize)
{
    if (jumpTable[key] == 0) return 0; // Invalid key or record doesn't exist
    flash_adr_t recAdr = SectorAdrs[sectorIdx] + (jumpTable[key] * BLOCK_SIZE);
    return readRecord(recAdr, target, bufferSize);
}

rec_size_t flashrec_size(rec_key_t key)
{
    if (jumpTable[key] == 0) return 0; // Invalid key or record doesn't exist
    flash_adr_t recAdr = SectorAdrs[sectorIdx] + (jumpTable[key] * BLOCK_SIZE);
    return getRecordSize(recAdr);
}

size_t flashrec_memUsed(void)
{
    return BLOCK_PAYLOAD_SIZE * blockUsage;
}

size_t flashrec_memTotal(void)
{
    return BLOCK_PAYLOAD_SIZE * (N_BLOCKS - 1);
}

//==============================================================================
// Local Functions
//==============================================================================

static int8_t findActiveSector(void)
{
    bool sectorValid[2];
    uint32_t sectorCycle[2];
    Block buffer;

    for (uint8_t i = 0; i < 2; ++i) {
        Block_readFromFlash(&buffer, SectorAdrs[i]);
        sectorValid[i] = Block_isCrcOk(&buffer);
        sectorCycle[i] = buffer.data.secHdr.cycle;
    }

    if (!sectorValid[1]) {
        if (sectorValid[0]) return 0;
        else return -1; // There is no valid sector
    }
    else if (!sectorValid[0]) return 1;
    // Both are valid, determine the most recent one
    else if (sectorCycle[0] > sectorCycle[1]) return 0;
    else return 1;
}

static blk_idx_t bytesToBlocks(rec_size_t size)
{
    blk_idx_t result = size / BLOCK_PAYLOAD_SIZE;
    if (result * BLOCK_PAYLOAD_SIZE != size) ++result;
    return result;
}

static void startMigration(rec_key_t skip)
{
    nextBlockIdx = 1;
    flash_adr_t srcSectorAdr = SectorAdrs[sectorIdx];
    flash_adr_t targetSectorAdr = SectorAdrs[(sectorIdx + 1) % 2];
    cleanEndAdr = targetSectorAdr;
    for (rec_key_t key = 1; key < FlashKey_nKeys; ++key) {
        if (key == skip) continue; // will be recorded after migration
        if (jumpTable[key] == 0) continue; // Record doesn't exist
        flash_adr_t srcRecAdr = srcSectorAdr + (jumpTable[key] * BLOCK_SIZE);
        rec_size_t recSize = getRecordSize(srcRecAdr);
        // No need to migrate zero-sized records
        if (recSize == 0) {
            jumpTable[key] = 0;
            continue;
        }
        flash_adr_t targetRecAdr = targetSectorAdr + (nextBlockIdx * BLOCK_SIZE);
        migrateRecord(srcRecAdr, targetRecAdr);
        jumpTable[key] = nextBlockIdx;
        nextBlockIdx += bytesToBlocks(recSize);
    }
    ++cycle;
    sectorIdx = (sectorIdx + 1) % 2;
}

static blk_idx_t calculateBlockUsage(void)
{
    blk_idx_t totalUsage = 0;
    for (rec_key_t key = 1; key < FlashKey_nKeys; ++key) {
        totalUsage += bytesToBlocks(flashrec_size(key));
    }
    return totalUsage;
}

//==============================================================================
// BlockBuffer Related Functions
//==============================================================================

static bool Block_isCrcOk(const Block* block)
{
    rec_crc_t calculatedCrc = calculate_crc(block, sizeof(block->data));
    return calculatedCrc == block->crc;
}

static bool Block_isClean(const Block* block, flash_adr_t adr)
{
    const byte* bytePtr = (const byte*)block;
    for (rec_size_t i = 0; i < BLOCK_SIZE; ++i) {
        if (bytePtr[i] != (uint8_t)FLASH_EMPTY_CELL) {
            if (((adr + i) % _FLASH_ERASE_SIZE == 0) && ((adr + i) % BLOCK_SIZE != 0)) {
                return true;
            }
            else return false;
        }
    }
    return true;
}

static enum BlockStatus Block_evaluate(const Block* block, flash_adr_t adr)
{
    if (Block_isClean(block, adr)) return BlockStatus_Clean;
    else if (!Block_isCrcOk(block)) return BlockStatus_Corrupted;
    else if (block->data.recSeg.header.cycle != (uint8_t)cycle) {
        return BlockStatus_Old;
    }
    else return BlockStatus_Valid;
}

static void Block_writeToFlash(const Block* block, flash_adr_t adr)
{
    // Erase on demand
    flash_adr_t blockEndAdr = adr + BLOCK_SIZE;
    while (blockEndAdr > cleanEndAdr) {
        flash2_eraseBlock(cleanEndAdr);
        cleanEndAdr += _FLASH_ERASE_SIZE;
    }

    flash2_write(adr, block, sizeof(Block));
}

//==============================================================================
// Record Related Functions
//==============================================================================

static rec_size_t getRecordSize(flash_adr_t recAdr)
{
    RecordHeader header;
    flash2_read(recAdr, &header, sizeof(header));
    return header.size;
}

static void writeRecord(flash_adr_t recAdr, rec_key_t key, const void* source,
        rec_size_t size)
{
    const uint8_t* srcPtr = (const uint8_t*)source;
    rec_size_t nBytesRemaining = size;
    Block buffer;

    while (nBytesRemaining != 0) {

        // Prepare & burn the write buffer
        memset(&buffer, FLASH_EMPTY_CELL, sizeof(buffer));
        rec_size_t bytesInBlock = (nBytesRemaining < BLOCK_PAYLOAD_SIZE) ?
                nBytesRemaining : BLOCK_PAYLOAD_SIZE;
        buffer.data.recSeg.header.cycle = (uint8_t)cycle;
        buffer.data.recSeg.header.key = key;
        buffer.data.recSeg.header.size = size;
        memcpy(buffer.data.recSeg.data, srcPtr, bytesInBlock);
        buffer.crc = calculate_crc(&buffer, sizeof(buffer.data));
        Block_writeToFlash(&buffer, recAdr);

        // Update the state
        recAdr += BLOCK_SIZE;
        nBytesRemaining -= bytesInBlock;
        srcPtr += bytesInBlock;
    }
}

static rec_size_t readRecord(flash_adr_t recAdr, void* target, size_t bufferSize)
{
    uint8_t* targetPtr = (uint8_t*)target;
    rec_size_t nBytesRemaining = getRecordSize(recAdr);
    Block buffer;

    if (bufferSize < nBytesRemaining) nBytesRemaining = (rec_size_t)bufferSize;
    rec_size_t retVal = nBytesRemaining;

    while (nBytesRemaining != 0) {
        rec_size_t bytesInBlock = (nBytesRemaining < BLOCK_PAYLOAD_SIZE) ?
                nBytesRemaining : BLOCK_PAYLOAD_SIZE;
        Block_readFromFlash(&buffer, recAdr);
        memcpy(targetPtr, buffer.data.recSeg.data, bytesInBlock);
        recAdr += BLOCK_SIZE;
        nBytesRemaining -= bytesInBlock;
        targetPtr += bytesInBlock;
    }
    return retVal;
}

static void migrateRecord(flash_adr_t srcAdr, flash_adr_t targetAdr)
{
    blk_idx_t nBlocksRemaning = bytesToBlocks(getRecordSize(srcAdr));
    Block buffer;

    while (nBlocksRemaning != 0) {
        Block_readFromFlash(&buffer, srcAdr);
        ++buffer.data.recSeg.header.cycle;
        buffer.crc = calculate_crc(&buffer, sizeof(buffer.data));
        Block_writeToFlash(&buffer, targetAdr);
        srcAdr += BLOCK_SIZE;
        targetAdr += BLOCK_SIZE;
        --nBlocksRemaning;
    }
}

//==============================================================================
// Sector Related Functions
//==============================================================================

static void writeSectorHeader(flash_adr_t secAdr)
{
    Block buffer;
    memset(&buffer, FLASH_EMPTY_CELL, sizeof(buffer));
    buffer.data.secHdr.cycle = cycle;
    buffer.crc = calculate_crc(&buffer, sizeof(buffer.data));
    Block_writeToFlash(&buffer, secAdr);
}

static uint32_t readSectorCycle(flash_adr_t secAdr)
{
    SectorHeader header;
    flash2_read(secAdr, &header, sizeof(header));
    return header.cycle;
}

