/**
 * @file kernel.h
 * @author Gokce Taglioglu
 */

#ifndef KERNEL_H
#define	KERNEL_H

#include "common_defs.h"
#include "event.h"

//==============================================================================
// Definitions & Settings
//==============================================================================

#define MACHINE_MAX_PRIORITY 1 // Starting from 0

struct Machine {
    void (*stateFunction)(Event*);
    uint8_t priority;
};
typedef struct Machine Machine;

//==============================================================================
// Global Functions
//==============================================================================

void kernel_initialize(void);
void kernel_push(Machine* machine, Event* event);
void kernel_pushFromISR(Machine* machine, Event* event);
Result kernel_pop(Machine** machine, Event** event);

#endif	/* KERNEL_H */

