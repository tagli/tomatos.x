/**
 * @file eventq.c
 * @author Gokce Taglioglu
 */

#include <stddef.h>
#include "eventq.h"

//==============================================================================
// Global Functions
//==============================================================================

void EventQ_create(EventQ* self, Event** buffer, qsize_t maxEvents)
{
    Queue_create(self, (byte*)buffer, sizeof(buffer[0]), maxEvents);
}

Result EventQ_push(EventQ* self, Event* event)
{
    Event_hold(event);
    Result result = Queue_send(self, &event);
    if (result == Result_Err) {
        Event_releaseDestroy(event);
    }
    return result;
}

Event* EventQ_pop(EventQ* self)
{
    Event* event = NULL;
    Queue_receive(self, &event);
    return event;
}

